export interface TooltipOpt {
    matTooltipShowDelay:number;
    matTooltipHideDelay:number;
}