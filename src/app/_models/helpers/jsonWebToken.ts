export interface JsonWebToken {
  userId: number;
  accessToken: string;
  accessLevel: number;
  expiresTicks: number;
}
