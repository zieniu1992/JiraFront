/** This is a register model */
export interface RegisterUser {
    email: string;
    name: string;
    password: string;
  }