export enum StatusNotification {
    Oczekuje_na_wsparcie = 0,
    Przypisane_do_użytkownika = 1,
    W_trakcie  = 2,
    Oczekuje_na_wsparcie_zespołu_zewnetrznego = 3,
    Oczekuje_na_klienta = 4,
    Rozwiazane = 5,
}