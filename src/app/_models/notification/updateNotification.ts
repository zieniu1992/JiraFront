import { Notification } from './notification';
import { StatusNotification } from './helpers/statusNotification';

export class UpdateNotification {
  id: number;
  type: number;
  title: string;
  assignedUserId: number;
  description: string;
  status: number;

  constructor(notification: Notification) {
    this.id = notification.id;
    this.description = notification.description;
    this.title = notification.title;
    this.type = notification.type;
    this.assignedUserId = notification.assignedUser !== undefined ? notification.assignedUser.id : 0;
    this.status = notification.status;
  }

  /** Przypisywanie osoby odpowiedzialnej za zgłoszenie */
  assignUser(assignedUser: number) {
    this.assignedUserId = assignedUser;

    if (this.status === StatusNotification.Oczekuje_na_wsparcie) {
      this.status = StatusNotification.Przypisane_do_użytkownika;
    }
  }

  /** Zmiana statusu zgłoszenia */
  changeStatus(statusNotification: StatusNotification) {
    this.status = statusNotification;
  }
}
