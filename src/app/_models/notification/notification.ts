import { StatusNotification } from './helpers/statusNotification';
import { User } from '../user/user';
import { NewNotification } from './newNotification';

export class Notification {
  id: number;
  type: number;
  title: string;
  declarantUser: User;
  assignedUser: User;
  description: string;
  status: StatusNotification;
  saveTime: Date;

  copyValues(_notification: Notification) {
    this.id = _notification.id;
    this.type = _notification.type;
    this.title = _notification.title;
    this.declarantUser = _notification.declarantUser;
    this.description = _notification.description;
    this.status = _notification.status;
    this.saveTime = _notification.saveTime;

    if (_notification.assignedUser !== null && undefined) {
      this.assignedUser = _notification.assignedUser;
    }
  }

}
