import { Notification } from './notification';

export class NewNotification {
  type: number;
  title: string;
  declarantUserId: number;
  description: string;

  constructor(notification:Notification) {
    this.type = notification.type;
    this.title = notification.title;
    this.declarantUserId = notification.declarantUser.id;
    this.description = notification.description;
  }
}
