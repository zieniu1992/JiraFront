import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot } from '@angular/router';
import * as decode from 'jwt-decode';
import { AuthenticationService } from '../_services/user/authentication/authentication.service';
import { JsonWebToken } from '../_models/helpers/jsonWebToken';
import { StorageUserService } from '../_services/helpers/storage-user.service';

@Injectable({
  providedIn: 'root',
})
export class RoleGuard implements CanActivate {
  constructor(private storageUserService:StorageUserService) {}

  canActivate(route: ActivatedRouteSnapshot): boolean {
    const expectedRole = route.data.expectedRole;
    const currentUser = this.storageUserService.getLoggedUser().accessLevel;

    if (currentUser < expectedRole) {
      return false;
    }
    return true;
  }
}
