import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/_services/user/authentication/authentication.service';
import { SnackBarStatusAction, SnackBarService } from 'src/app/_services/helpers/snack-bar.service';
import { User } from 'src/app/_models/user/user';
import { StorageUserService } from 'src/app/_services/helpers/storage-user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loading = false; // oczekiwanie na zalogowanie
  passHide = true; // ukrywanie hasła
  returnUrl: string;

  loginForm = this.formBuilder.group({
    // formatka logowania
    email: ['', Validators.required],
    password: ['', Validators.required],
  });

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private snackBarService: SnackBarService,
    private storageUserService: StorageUserService
  ) {}

  ngOnInit() {
    // reset login status
    this.authenticationService.logout();

    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || '/';
  }

  getCheckRequired(): boolean {
    // jezeli pola nie sa wypilnione to przycisk jest nieaktywny
    if (this.loginForm.invalid) {
      return true;
    } else {
      return false;
    }
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.loginForm.controls;
  }

  /** Potwierdzanie logowania */
  signIn() {
    this.loading = true;
    this.authenticationService
      .signIn(this.f.email.value, this.f.password.value)
      .pipe(first())
      .subscribe(
        (src) => {
          this.router.navigate(['/pages']);
        },
        (error) => {
          this.loading = false;
          this.snackBarService.openSnackBar('Logowanie nieudane.', '', SnackBarStatusAction.Error);
        }
      );
  }

  /** Przekierowanie do rejestracji */
  goToRegister() {
    this.router.navigateByUrl('/register');
  }
}
