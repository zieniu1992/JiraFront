import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserHttpService } from 'src/app/_services/user/user-http.service';
import { RegisterUser } from 'src/app/_models/user/registerUser';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { SnackBarService, SnackBarStatusAction } from 'src/app/_services/helpers/snack-bar.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  loading = false; // oczekiwanie na zarejestrowanie
  passHide = true; // ukrywanie hasła

  registerForm = this.formBuilder.group({ // formatka rejestracji
    email: ['', [Validators.required, Validators.email]],
    name: ['', Validators.required],
    password: ['', [Validators.required, Validators.minLength(6)]],
    rePassword: ['', [Validators.required, Validators.minLength(6)]],
  });

  constructor(private formBuilder: FormBuilder, private userHttpService: UserHttpService, private router: Router,
    private location: Location, private snackBarService: SnackBarService) { }

  ngOnInit(): void {
  }

  /**
   * Sprawdzanie wymaganych pol
   */
  checkValidRequired(): boolean {
    if (this.registerForm.invalid) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Sprawdzanie poprawnosci hasła
   */
  checkValidPassword(): boolean {
    if (this.passwordForm.value !== this.rePasswordForm.value ||
      (this.passwordForm.hasError('required') === true || this.rePasswordForm.hasError('required') === true)) {
      return false;
    } else {
      return true;
    }
  }

  /**
   * Rejestracja nowego użytkownika
   */
  register() {
    const newUser: RegisterUser = {
      email: this.emailForm.value,
      name: this.registerForm.controls.name.value,
      password: this.passwordForm.value
    };

    this.userHttpService.registerUser(newUser).subscribe(
      (src) => {
        this.router.navigate(['./login']);
        this.snackBarService.openSnackBar('Udana rejestracja użytkownika.', '', SnackBarStatusAction.Success);
      },
      (error) => {
        this.snackBarService.openSnackBar('Nieudana rejestracja użytkownika.', '', SnackBarStatusAction.Error);
      }
    );
  }

  /** 
   * Powrót do poprzedniej lokalizacji
   */
  back() {
    this.location.back();
  }

  get emailForm() { return this.registerForm.controls.email; } // formatka emaila
  get passwordForm() { return this.registerForm.controls.password; } // formatka hasła
  get rePasswordForm() { return this.registerForm.controls.rePassword; } // formatka powtorzenia hasla
}
