import { NgModule } from '@angular/core';
import { PagesComponent } from './pages.component';
import { PagesRoutingModule } from './pages-routing.module';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { MatSidenavModule } from '@angular/material/sidenav';
import { ToolbarModule } from '../components/toolbar/toolbar.module';
import { MatListModule } from '@angular/material/list';

@NgModule({
    declarations: [
        PagesComponent,
    ],
    imports: [
        CommonModule,
        FormsModule,
        ToolbarModule,
        MatIconModule,
        MatListModule,
        MatSidenavModule,
        FontAwesomeModule,
        PagesRoutingModule,
    ]
})

export class PagesModule { }