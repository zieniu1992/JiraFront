import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UserPermission } from 'src/app/_models/user/helpers/userPermission';

@Component({
  templateUrl: './user-change-permission-dialog.component.html',
  styleUrls: ['./user-change-permission-dialog.component.scss']
})
export class UserChangePermissionDialogComponent implements OnInit {

  permissions = UserPermission; // spis uprawnien
  permissionForm: FormControl = new FormControl('', Validators.required);

  constructor(private formBuilder: FormBuilder, public dialogRef: MatDialogRef<UserChangePermissionDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    this.permissionForm.setValue(this.data.accessLevel);
  }

  /**
   * Zamykanie okna dialogowego bez wprowadzania zmian
   */
  closeDialog() {
    this.dialogRef.close();
  }

  /**
   * Potwierdzenie uprawnienia
   */
  confirmPermission() {
    this.dialogRef.close(this.permissionForm.value);
  }

  /** Porównywanie obiektów i przypisanie odpowiedniego do formatki */
  compareObject(obj1, obj2) {
    return parseInt(obj1, 10) === obj2;
  }
}
