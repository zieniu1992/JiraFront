import { Component, OnInit } from '@angular/core';
import { MatBottomSheetRef } from '@angular/material/bottom-sheet';
import { Router, ActivatedRoute } from '@angular/router';
import { StorageUserService } from 'src/app/_services/helpers/storage-user.service';

@Component({
  templateUrl: './user-bottom-list.component.html',
  styleUrls: ['./user-bottom-list.component.scss'],
})
export class UserBottomListComponent implements OnInit {
  userId: number; // id uzytkownika

  constructor(
    private _bottomSheetRef: MatBottomSheetRef<UserBottomListComponent>,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private storageUserService: StorageUserService
  ) {}

  ngOnInit(): void {
    this.userId = this.storageUserService.getLoggedUser().userId; // pobranie id uzytkownika
  }

  /** Przejscie do nowej karty */
  openNavigationLink(link: string) {
    this._bottomSheetRef.dismiss();
    const userId = this.userId;

    this.router.navigate([`/pages/${link}`], {
      queryParams: { userId },
      relativeTo: this.activatedRoute,
    });
  }

  /** Przejscie do nowej karty */
  openUserLink() {
    this._bottomSheetRef.dismiss();
    const edit = true;
    const owner = true;

    this.router.navigate([`/pages/user/${this.userId}`], {
      queryParams: { edit, owner },
      relativeTo: this.activatedRoute,
    });
  }
}
