import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserBottomListComponent } from './user-bottom-list.component';

describe('UserBottomListComponent', () => {
  let component: UserBottomListComponent;
  let fixture: ComponentFixture<UserBottomListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserBottomListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserBottomListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
