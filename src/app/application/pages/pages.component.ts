import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MediaMatcher } from '@angular/cdk/layout';
import { slideInOutAnimation } from 'src/app/_animation/slideInOutAnimation';
import { RouterOutlet } from '@angular/router';
import { fadeInAnimation } from 'src/app/_animation/fadeInAnimation';
import { StorageUserService } from 'src/app/_services/helpers/storage-user.service';
import { UserHttpService } from 'src/app/_services/user/user-http.service';

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss'],
  animations: [slideInOutAnimation, fadeInAnimation],
  // attach the slide in/out animation to the host (root) element of this component
  host: { '[@slideInOutAnimation]': '' },
})
export class PagesComponent implements OnInit, OnDestroy {
  mobileQuery: MediaQueryList;
  display = false; // zmienna okreslajaca wyswietlanie sideNav
  rotate = false; // zmienna okreslajaca obracanie menubar w sideNav
  hide = true;
  openSidenav = false; // określanie czy sidenav jest otwarty

  mode = new FormControl('over');

  // ###############################################
  // ################## SUBMENU ####################
  showSubmenuCart = false; // otwieranie submenu cart
  showSubmenuContent = false; // otwieranie submenu content
  showSubmenuSettings = false; // otwieranie submenu settings
  // ###############################################
  private mobileQueryListener: () => void;

  constructor(
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    private storageUserService: StorageUserService,
    private userHttpService: UserHttpService
  ) {
    this.mobileQuery = media.matchMedia('(max-width: 100px)');
    this.mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addEventListener('change', this.mobileQueryListener);
  }
  ngOnInit() {
    this.getUser();
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeEventListener('change', this.mobileQueryListener);
  }

  private getUser() {
    this.userHttpService
      .getUserById(this.storageUserService.getLoggedUser().userId)
      .subscribe((next) => this.storageUserService.setUserName(next.name));
  }

  /** Zmiana animacji */
  prepareRoute(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData.animation;
  }
}
