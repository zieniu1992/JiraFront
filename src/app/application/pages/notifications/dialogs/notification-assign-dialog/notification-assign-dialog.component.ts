import { Component, OnInit, Inject } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { User } from 'src/app/_models/user/user';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UserHttpService } from 'src/app/_services/user/user-http.service';

@Component({
  templateUrl: './notification-assign-dialog.component.html',
  styleUrls: ['./notification-assign-dialog.component.scss'],
})
export class NotificationAssignDialogComponent implements OnInit {
  userControl = new FormControl('', Validators.required); // formatka odpowiedzialna za wybór użytkownika
  users: Array<User>; //
  filteredUser: Observable<User[]>;

  constructor(
    private userHttpService: UserHttpService,
    public dialogRef: MatDialogRef<NotificationAssignDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {
    this.getUsers();
  }


  /** Zatwierdzanie zmian */
  confirmChanges() {
    this.dialogRef.close(this.userControl.value);
  }

  /** Wyswietlanie stringa uzywajac obiektu */
  displayFn(user: User): string {
    // wyswietlanie po obiekcie
    return user && user.name ? user.name : '';
  }

  private getUsers() {
    this.userHttpService.getUsers().subscribe((next) => {
      this.users = next;
    },(error) => {

    },
    () => {
      this.filterUser(); // subskrypcja formatki
    });
  }

  /** Filtrowanie uzytkownikow */
  private filterUser() {
    this.filteredUser = this.userControl.valueChanges.pipe(
      // filtrowanie uzytkownikow
      startWith(''),
      map((value) => (typeof value === 'string' ? value : value.name)),
      map((name) => (name ? this._filter(name) : this.users.slice()))
    );
  }

  private _filter(value: string): User[] {
    // filtrowanie uzytkownikow
    const filterValue = value.toLowerCase();

    return this.users.filter((option) => option.name.toLowerCase().indexOf(filterValue) === 0);
  }
}
