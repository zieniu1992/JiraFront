import { Component, OnInit, Inject } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { StatusNotification } from 'src/app/_models/notification/helpers/statusNotification';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  templateUrl: './notification-change-status-dialog.component.html',
  styleUrls: ['./notification-change-status-dialog.component.scss'],
})
export class NotificationChangeStatusDialogComponent implements OnInit {
  statusForm: FormControl = new FormControl('', Validators.required); // formatka statusu
  statuses = StatusNotification; // typ wyliczeniowy statusow

  constructor(
    public dialogRef: MatDialogRef<NotificationChangeStatusDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit(): void {
    this.setStatusForm();
  }

  /** Zatwierdzanie zmian */
  confirmChanges() {
    this.dialogRef.close(parseInt(this.statusForm.value,10));
  }

  /** Porównywanie obiektów i przypisanie odpowiedniego do formatki */
  compareObject(obj1, obj2) {
    return parseInt(obj1, 10) === obj2;
  }

  /** Ustawianie statusu formaki */
  private setStatusForm() {
    this.statusForm.setValue(this.data.notification.status);
  }
}
