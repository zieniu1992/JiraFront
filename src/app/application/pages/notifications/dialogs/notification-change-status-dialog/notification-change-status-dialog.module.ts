import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotificationChangeStatusDialogComponent } from './notification-change-status-dialog.component';
import { AngularMaterialModule } from 'src/app/_modules/angular-material.module';
import { DragAndDropModule } from 'src/app/application/components/drag-and-drop/drag-and-drop.module';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/_shared/shared.module';

@NgModule({
  declarations: [NotificationChangeStatusDialogComponent],
  imports: [CommonModule, AngularMaterialModule, DragAndDropModule, ReactiveFormsModule, SharedModule],
  entryComponents: [NotificationChangeStatusDialogComponent],
})
export class NotificationChangeStatusDialogModule {}
