import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotificationCreateNewDialogComponent } from './notification-create-new-dialog.component';
import { AngularMaterialModule } from 'src/app/_modules/angular-material.module';
import { DragAndDropModule } from 'src/app/application/components/drag-and-drop/drag-and-drop.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NotificationMoreInfoModule } from '../../helpers/notification-more-info/notification-more-info.module';

@NgModule({
  declarations: [NotificationCreateNewDialogComponent],
  imports: [
    CommonModule,
    FormsModule,
    DragAndDropModule,
    ReactiveFormsModule,
    AngularMaterialModule,
    NotificationMoreInfoModule,
  ],
  entryComponents: [NotificationCreateNewDialogComponent],
})
export class NotificationCreateNewDialogModule {}
