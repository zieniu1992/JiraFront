import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NewNotification } from 'src/app/_models/notification/newNotification';
import { Notification } from 'src/app/_models/notification/notification';

@Component({
  templateUrl: './notification-create-new-dialog.component.html',
  styleUrls: ['./notification-create-new-dialog.component.scss'],
})
export class NotificationCreateNewDialogComponent implements OnInit {
  editable = true; // tryb edycji
  showToolbar = true; // pokaż toolbar

  notification: Notification; // obiekt zgloszenia

  constructor(
    public dialogRef: MatDialogRef<NotificationCreateNewDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit(): void {}

  /** Odbieranie zmian z dziecka */
  notificationChanges(notification: Notification) {
    this.notification = notification;
  }

  /**
   * Zamykanie okna dialogowego bez wprowadzania zmian
   */
  closeDialog() {
    this.dialogRef.close();
  }

  /**
   * Potwierdzenie zgloszenia
   */
  confirmNotification() {
    const newNotification = new NewNotification(this.notification);
    this.dialogRef.close(newNotification);
  }
}
