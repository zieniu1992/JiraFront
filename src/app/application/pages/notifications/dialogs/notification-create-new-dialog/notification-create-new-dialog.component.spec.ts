import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificationCreateNewDialogComponent } from './notification-create-new-dialog.component';

describe('NotificationMoreInfoComponent', () => {
  let component: NotificationCreateNewDialogComponent;
  let fixture: ComponentFixture<NotificationCreateNewDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotificationCreateNewDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationCreateNewDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
