import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { UserPermission } from 'src/app/_models/user/helpers/userPermission';
import { SnackBarService, SnackBarStatusAction } from 'src/app/_services/helpers/snack-bar.service';
import { RoleGuardService } from 'src/app/_services/user/authentication/role-guard.service';
import { ConfirmDialogComponent } from 'src/app/application/components/confirm-dialog';
import { MatDialog } from '@angular/material/dialog';
import { NotificationCommentsHttpService } from 'src/app/_services/notification/comments/notification-comments-http.service';
import { FormControl } from '@angular/forms';
import { StorageUserService } from 'src/app/_services/helpers/storage-user.service';
import { NotiComment } from 'src/app/_models/notification/comments/notiComment';
import { NewNotiComment } from 'src/app/_models/notification/comments/newNotiComment';
import { Notification } from 'src/app/_models/notification/notification';
import { SignalRService } from 'src/app/_services/helpers/signal-r.service';
import { HubConnection } from '@microsoft/signalr';

@Component({
  selector: 'app-notification-comments',
  templateUrl: './notification-comments.component.html',
  styleUrls: ['./notification-comments.component.scss'],
})
export class NotificationCommentsComponent implements OnInit {
  @Input() notification: Notification;

  numberOfPage = 0; // aktualna strona wiadomości
  showDelayTooltip = 500;
  hideDelayTooltip = 500;

  notificationHub: HubConnection;
  /** ************************ ROLE ********************** */
  permission = UserPermission;
  /** **************************************************** */

  messages: Array<NotiComment> = new Array<NotiComment>(); // wiadomosci
  messageForm: FormControl = new FormControl(); // formatka odpowiedzialna za tresc nowej wiadomsoci

  constructor(
    private snackBarService: SnackBarService,
    private notificationCommentsHttpService: NotificationCommentsHttpService,
    public roleGuardService: RoleGuardService,
    private storageUserService: StorageUserService,
    private signalRService: SignalRService,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.getMessages(this.numberOfPage, this.notification.id);
    this.connectToServer();
    this.detectChangesInSignalr();
  }

  onScrollUp(): void {
    this.numberOfPage++;
    this.getMessages(this.numberOfPage, this.notification.id);
  }

  /**
   * Pobieranie wiadomosci z serwera
   * @param page aktualny numer strony wiadomosci
   */
  getMessages(page: number, notificationId: number) {
    this.notificationCommentsHttpService.getCommentsByNotificationAndPage(page, notificationId).subscribe(
      (src) => {
        this.messages = this.messages.concat(src);        
      },
      (error) => {
        this.snackBarService.openSnackBar('Nieudane pobranie wiadomości.', '', 'snackBar-error');
      }
    );
  }


  /** Usuwanie wiadomosci */
  deleteMessage(message: NotiComment) {
    const header = 'Potwierdzenie';
    const content = 'Czy aby napewno chcesz usunąć wiadomość?';

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: { header, content },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.notificationCommentsHttpService.deleteComment(message.id).subscribe(
          (next) => {
            this.snackBarService.openSnackBar('Udane usunięcie wiadomości', '', SnackBarStatusAction.Success);
            this.notificationHub.invoke('DeleteCommentAsync', message.id, this.notification.id.toString());
          },
          (error) => {
            this.snackBarService.openSnackBar('Nieudane usunięcie wiadomości', '', SnackBarStatusAction.Error);
          }
        );
      }
    });
  }

  /** Tworzenie nowej wiadomosci */
  createNewMessage() {
    const newMessage: NewNotiComment = {
      description: this.messageForm.value,
      userId: this.storageUserService.getLoggedUser().userId,
      notificationId: this.notification.id,
    };

    this.notificationCommentsHttpService.addNewComment(newMessage).subscribe(
      (next) => {
        const message: NotiComment = {
          description: newMessage.description,
          userName: this.storageUserService.getUserName(),
          saveTime: new Date(),
        };

        this.notificationHub.invoke('CreateCommentAsync', message, this.notification.id.toString());
        this.messageForm.setValue('');
        this.snackBarService.openSnackBar('Udane dodanie nowego komentarza', '', SnackBarStatusAction.Success);
      },
      (error) => {
        this.snackBarService.openSnackBar('Nieudane dodanie nowej wiadomosci', '', SnackBarStatusAction.Error);
      }
    );
  }

  private connectToServer() {
    this.notificationHub = this.signalRService.connectToServer('noti', () => {
      this.notificationHub.invoke('AddToGroupAsync', this.notification.id.toString());
    });
  }

  private detectChangesInSignalr() {
    this.notificationHub.on('CreateCommentAsync', (message: NotiComment) => {
      this.messages.push(message);
    });

    this.notificationHub.on('DeleteCommentAsync', (messageId: number) => {
      const index = this.messages.findIndex((s) => s.id === messageId);

      if (index >= 0) {
        this.messages.splice(index, 1);
      }
    });
  }
}
