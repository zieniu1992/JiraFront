import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotificationCommentsComponent } from '.';
import { AngularMaterialModule } from 'src/app/_modules/angular-material.module';
import { NgxInfiniteScrollerModule } from 'ngx-infinite-scroller';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/_shared/shared.module';

@NgModule({
  declarations: [NotificationCommentsComponent],
  imports: [CommonModule, SharedModule, ReactiveFormsModule, AngularMaterialModule, NgxInfiniteScrollerModule],
  exports: [NotificationCommentsComponent],
})
export class NotificationCommentsModule {}
