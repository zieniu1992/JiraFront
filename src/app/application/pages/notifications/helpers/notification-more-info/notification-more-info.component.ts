import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { SelectItem } from 'src/app/_models/helpers/selectItem';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { Notification } from '../../../../../_models/notification/notification';
import { Subscription } from 'rxjs';
import { StorageUserService } from 'src/app/_services/helpers/storage-user.service';
import { UserHttpService } from 'src/app/_services/user/user-http.service';
import { User } from 'src/app/_models/user/user';
import { SnackBarService, SnackBarStatusAction } from 'src/app/_services/helpers/snack-bar.service';

@Component({
  selector: 'app-notification-more-info',
  templateUrl: './notification-more-info.component.html',
  styleUrls: ['./notification-more-info.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NotificationMoreInfoComponent implements OnInit, OnDestroy {
  @Input() notification: Notification = new Notification(); // obiekt zgloszenia
  @Input() editable: boolean; // tryb edycji
  @Input() showToolbar: boolean; // toolbar edytora
  @Output() notificationChanges: EventEmitter<Notification> = new EventEmitter();

  notificationForm = this.formBuilder.group({
    project: [0, Validators.required],
    type: ['', Validators.required],
    title: ['', Validators.required],
    user: ['', Validators.required],
    description: [''],
  });

  user: User; // użytkownik deklarujacy zgłoszenie

  projects: SelectItem[] = [{ value: 0, viewValue: 'PP - JIRA' }]; // nazwa projektu
  types: SelectItem[] = [
    // typ zgloszenia
    { value: 0, viewValue: 'Incydent' },
    { value: 1, viewValue: 'Żądanie' },
  ];

  editorConfig: AngularEditorConfig = {
    // konfiguracja edytora
    editable: false,
    spellcheck: false,
    height: '300px',
    width: 'auto',
    enableToolbar: true,
    showToolbar: true,
    placeholder: 'Enter text here...',
    fonts: [
      { class: 'arial', name: 'Arial' },
      { class: 'times-new-roman', name: 'Times New Roman' },
      { class: 'calibri', name: 'Calibri' },
      { class: 'comic-sans-ms', name: 'Comic Sans MS' },
    ],
    sanitize: true,
    toolbarPosition: 'top',
    toolbarHiddenButtons: [
      [
        'undo',
        'redo',
        'link',
        'unlink',
        'customClasses',
        'insertImage',
        'insertVideo',
        'insertHorizontalRule',
        'removeFormat',
        'toggleEditorMode',
      ],
    ],
  };

  /** ####################### SUBSCRIPTION ######################## */
  notificationChangesSub: Subscription;
  /** ############################################################# */
  constructor(
    private formBuilder: FormBuilder,
    private storageUserService: StorageUserService,
    private userHttpService: UserHttpService,
    private snackBarService: SnackBarService
  ) {}

  ngOnInit(): void {
    this.detectNewOrEdit();
    this.setForm();
    this.initEditor();
    this.emitValueChangesForm();

    this.detectFocusEditorAngular();
  }

  ngOnDestroy(): void {
    if (this.notificationChangesSub !== undefined) {
      this.notificationChangesSub.unsubscribe();
    }
  }

  /** Sprawdzanie czy mamy doczynienia z tworzeniem bądż podgłądem zgłoszenia */
  private detectNewOrEdit() {
    if (this.notification.declarantUser === undefined) {
      this.getUserById();
    }
  }

  /** Pobieranie uzytkownika z bazy danych */
  private getUserById() {
    const userId = this.storageUserService.getLoggedUser().userId;

    this.userHttpService.getUserById(userId).subscribe(
      (next) => {
        if (next !== null) {
          this.user = next;
          this.notification.declarantUser = this.user;
          this.notificationForm.controls.user.setValue(this.user.name);
        }
      },
      (error) => {
        this.snackBarService.openSnackBar('Nieudane pobranie użytkownika', '', SnackBarStatusAction.Error);
      }
    );
  }

  /** Wykrywanie zaznaczenia edytora  */
  private detectFocusEditorAngular() {
    const editor = document.getElementById('angularEditor');
    editor.addEventListener('focusin', () => {
      this.inEditMode(true);
    });
    editor.addEventListener('focusout', () => {
      this.inEditMode(false);
    });
  }

  /** Określanie trybu edycji */
  private inEditMode(status: boolean) {
    this.editorConfig.editable = status ? (this.editorConfig.editable = true) : (this.editorConfig.editable = false);
    this.editorConfig.showToolbar = status
      ? (this.editorConfig.showToolbar = true)
      : (this.editorConfig.showToolbar = false);
  }

  /** Inicjalizacja edytora */
  private initEditor() {
    if (this.editable === false) {
      this.editorConfig.editable = false;
      // this.notificationForm.disable();
    }
    if (this.showToolbar === false) {
      this.editorConfig.showToolbar = false;
    }
  }

  /** Inicjalizacja formatki */
  private setForm() {
    this.notificationForm.controls.type.setValue(this.notification.type);
    this.notificationForm.controls.title.setValue(this.notification.title);
    this.notificationForm.controls.user.setValue(this.notification?.declarantUser?.name);
    this.notificationForm.controls.description.setValue(this.notification.description);
  }

  /** Emitowanie zmian do rodzica */
  private emitValueChangesForm() {
    this.notificationChangesSub = this.notificationForm.valueChanges.subscribe((result: Notification) => {
      result.declarantUser = this.notification.declarantUser;
      result.id = this.notification.id;
      // result.assignedUser
      this.notificationChanges.next(result);
    });
  }
}
