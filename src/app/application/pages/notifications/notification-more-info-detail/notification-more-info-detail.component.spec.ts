import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificationMoreInfoDetailComponent } from './notification-more-info-detail.component';

describe('NotificationMoreInfoDetailComponent', () => {
  let component: NotificationMoreInfoDetailComponent;
  let fixture: ComponentFixture<NotificationMoreInfoDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotificationMoreInfoDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationMoreInfoDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
