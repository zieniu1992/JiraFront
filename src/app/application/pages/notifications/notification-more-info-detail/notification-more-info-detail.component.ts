import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { NotificationStatusService } from 'src/app/_services/notification/notification-status.service';
import { StatusNotification } from 'src/app/_models/notification/helpers/statusNotification';
import { NotificationChangeStatusDialogComponent } from '../dialogs/notification-change-status-dialog';
import { UpdateNotification } from 'src/app/_models/notification/updateNotification';
import { Notification } from '../../../../_models/notification/notification';
import { NotificationHttpService } from 'src/app/_services/notification/notification-http.service';
import { SnackBarService, SnackBarStatusAction } from 'src/app/_services/helpers/snack-bar.service';
import { NotificationAssignDialogComponent } from '../dialogs/notification-assign-dialog';
import { User } from 'src/app/_models/user/user';
import { NotificationService } from 'src/app/_services/notification/notification.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { ConfirmDialogComponent } from 'src/app/application/components/confirm-dialog';
import { RoleGuardService } from 'src/app/_services/user/authentication/role-guard.service';
import { UserPermission } from 'src/app/_models/user/helpers/userPermission';

@Component({
  templateUrl: './notification-more-info-detail.component.html',
  styleUrls: ['./notification-more-info-detail.component.scss'],
})
export class NotificationMoreInfoDetailComponent implements OnInit, OnDestroy {
  editable = false; // tryb edycji
  showToolbar = false; // pokaż toolbar

  status = StatusNotification; // typ wyliczeniowy okreslajacy status zgloszenia
  notification: Notification = new Notification(); // obiekt zgloszenia
  inEditMode = false; // wykrywanie zmian

  userPermission = UserPermission;

  matTooltipShowDelay = 500; // opoznienie w wyswietlenie podpowiedzi
  matTooltipHideDelay = 300; // opoznienie w ukryciu podpowiedzi

  /** **************** SUBSCRIPTION ***************** */
  getNotificationSub: Subscription;
  /** *********************************************** */

  constructor(
    private dialog: MatDialog,
    private location: Location,
    private notificationService: NotificationService,
    public notificationStatusService: NotificationStatusService,
    private notificationHttpService: NotificationHttpService,
    private snackBarService: SnackBarService,
    public roleGuardService:RoleGuardService
  ) {}

  ngOnInit(): void {
    this.getCurrentNotification();
  }

  ngOnDestroy(): void {
    if (this.getNotificationSub !== undefined) {
      this.getNotificationSub.unsubscribe();
    }
  }

  /** Powrót do poprzedniej lokalizacji */
  goBack() {
    this.location.back();
  }

  /** Odbieranie zmian z dziecka */
  notificationChanges(notification: Notification) {    
    this.notification.copyValues(notification);
    this.inEditMode = true;
  }

  /** Zmiana statusu zgloszenia */
  openChangeNotificationStatus(notification: Notification): any {
    const dialogRef = this.dialog.open(NotificationChangeStatusDialogComponent, {
      data: { notification },
      width: '500px',
      hasBackdrop: true,
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        const updatedNotification = new UpdateNotification(notification);
        updatedNotification.changeStatus(result);
        this.notificationHttpService.updateNotification(updatedNotification).subscribe(
          (next) => {
            this.snackBarService.openSnackBar(
              `Udane aktualizacja statusu ${notification.title}`,
              '',
              SnackBarStatusAction.Success
            );
            notification.status = parseInt(result,10);
          },
          (error) => {
            this.snackBarService.openSnackBar('Nieudana aktualizacja statusu', '', SnackBarStatusAction.Error);
          }
        );
      }
    });
  }

  /** Przypisywanie osoby do zgłoszenia */
  openAssignUserDialog(notification: Notification) {
    const dialogRef = this.dialog.open(NotificationAssignDialogComponent, {
      width: '450px',
      data: {},
    });

    dialogRef.afterClosed().subscribe((result: User) => {
      if (result) {
        const updatedNotification = new UpdateNotification(notification);
        updatedNotification.assignUser(result.id);
        this.notificationHttpService.updateNotification(updatedNotification).subscribe(
          (next) => {
            notification.assignedUser = result; // przypisywanie uzytkownika
            if (notification.status === StatusNotification.Oczekuje_na_wsparcie) {
              notification.status = StatusNotification.W_trakcie;
            }
            this.snackBarService.openSnackBar('Udana aktualizacja zgłoszenia', '', SnackBarStatusAction.Success);
          },
          (error) => {
            this.snackBarService.openSnackBar('Nieudana aktualizacja zgłoszenia', '', SnackBarStatusAction.Error);
          }
        );
      }
    });
  }

  /** Aktualizacja zgłoszenia */
  openUpdateConfirmDialog(notification: Notification) {
    const header = 'Potwierdzenie';
    const content = 'Czy aby napewno chcesz zaktualizwać zgłoszenie?';

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: { header, content },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        const updatedNotification = new UpdateNotification(notification);

        this.notificationHttpService.updateNotification(updatedNotification).subscribe(
          (next) => {
            this.inEditMode = false;
            this.snackBarService.openSnackBar('Udana aktualizacja zgłoszenia', '', SnackBarStatusAction.Success);
          },
          (error) => {
            this.snackBarService.openSnackBar('Nieudana aktualizacja zgłoszenia', '', SnackBarStatusAction.Error);
          }
        );
      }
    });
  }

  /** Usuwanie zgłoszenia */
  openDeleteDialog(id: number) {
    const header = 'Potwierdzenie';
    const content = 'Czy aby napewno chcesz usunąć zgłoszenie?';

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: { header, content },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.notificationHttpService.deleteNotification(id).subscribe(
          (next) => {
            this.snackBarService.openSnackBar('Udane usunięcie zgłoszenia', '', SnackBarStatusAction.Success);
            setTimeout(() => this.goBack(), 3000); // powrót do poprzedniej lokalizacji
          },
          (error) => {
            this.snackBarService.openSnackBar('Nieudane usunięcie zgłoszenia', '', SnackBarStatusAction.Error);
          }
        );
      }
    });
  }

  /** Pobieranie aktualnie wybranego zgloszenia */
  private getCurrentNotification() {
    this.getNotificationSub = this.notificationService.currentSelectedNotification$.subscribe((next) => {
      if (next !== null) {
        this.notification = next;
      }
    });
  }
}
