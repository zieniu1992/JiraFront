import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificationsTabsComponent } from './notifications-tabs.component';

describe('NotificationsTabsComponent', () => {
  let component: NotificationsTabsComponent;
  let fixture: ComponentFixture<NotificationsTabsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotificationsTabsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationsTabsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
