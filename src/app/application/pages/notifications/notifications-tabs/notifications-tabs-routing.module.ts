import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotificationsTabsComponent } from './notifications-tabs.component';

const routes: Routes = [
  {
    path: '',
    component: NotificationsTabsComponent,
    data: { animation: 'notTabs' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NotificationsTabsRoutingModule {}
