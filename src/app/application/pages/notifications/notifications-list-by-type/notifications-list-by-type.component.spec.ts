import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificationsListByTypeComponent } from './notifications-list-by-type.component';

describe('NotificationsListByTypeComponent', () => {
  let component: NotificationsListByTypeComponent;
  let fixture: ComponentFixture<NotificationsListByTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotificationsListByTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationsListByTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
