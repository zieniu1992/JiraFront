import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotificationsListByTypeComponent } from '.';
import { AngularMaterialModule } from 'src/app/_modules/angular-material.module';
import { SharedModule } from 'src/app/_shared/shared.module';
import { NotificationAssignDialogModule } from '../dialogs/notification-assign-dialog/notification-assign-dialog.module';
import { NotificationCreateNewDialogModule } from '../dialogs/notification-create-new-dialog/notification-create-new-dialog.module';
import { NotificationChangeStatusDialogModule } from '../dialogs/notification-change-status-dialog/notification-change-status-dialog.module';
import { NotificationStatusService } from 'src/app/_services/notification/notification-status.service';
import { NotificationsListByTypeRoutingModule } from './notifications-list-by-type-routing.module';

@NgModule({
  declarations: [NotificationsListByTypeComponent],
  imports: [
    CommonModule,
    AngularMaterialModule,
    SharedModule,
    NotificationsListByTypeRoutingModule,
    NotificationAssignDialogModule,
    NotificationCreateNewDialogModule,
    NotificationChangeStatusDialogModule,
  ],
  providers: [NotificationStatusService],
  exports: [NotificationsListByTypeComponent],
})
export class NotificationsListByTypeModule {}
