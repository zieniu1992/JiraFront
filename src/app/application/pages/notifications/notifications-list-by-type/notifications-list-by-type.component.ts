import { Component, OnInit, ChangeDetectionStrategy, ViewChild, Input } from '@angular/core';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { StatusNotification } from 'src/app/_models/notification/helpers/statusNotification';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { SnackBarService, SnackBarStatusAction } from 'src/app/_services/helpers/snack-bar.service';
import { NotificationService } from 'src/app/_services/notification/notification.service';
import { NotificationHttpService } from 'src/app/_services/notification/notification-http.service';
import { NotificationStatusService } from 'src/app/_services/notification/notification-status.service';
import { NotificationAssignDialogComponent } from '../dialogs/notification-assign-dialog';
import { User } from 'src/app/_models/user/user';
import { UpdateNotification } from 'src/app/_models/notification/updateNotification';
import { ConfirmDialogComponent } from 'src/app/application/components/confirm-dialog';
import { Notification } from './../../../../_models/notification/notification';
import { StorageUserService } from 'src/app/_services/helpers/storage-user.service';
import { RoleGuardService } from 'src/app/_services/user/authentication/role-guard.service';
import { UserPermission } from 'src/app/_models/user/helpers/userPermission';
import { Location } from '@angular/common';
import { NotificationChangeStatusDialogComponent } from '../dialogs/notification-change-status-dialog';

/** Typ zgłoszeń (wszystkie,zgłaszane,przypisane) */
export enum TypeNotification {
  All = 0,
  Published = 1,
  Assigned = 2,
}

@Component({
  selector: 'app-notifications-list-by-type',
  templateUrl: './notifications-list-by-type.component.html',
  styleUrls: ['./notifications-list-by-type.component.scss']
})
export class NotificationsListByTypeComponent implements OnInit {
  matTooltipShowDelay = 500; // opoznienie w wyswietlenie podpowiedzi
  matTooltipHideDelay = 300; // opoznienie w ukryciu podpowiedzi
  header: string; // nagłówek

  displayedColumns = ['id', 'type', 'title', 'assignedUser', 'declarantUser', 'buttons']; // kolumny

  dataSource: MatTableDataSource<Notification>; // dane tabularyczne
  notifications: Array<Notification>; // spis zgloszeń
  status = StatusNotification; // status zgłoszenia
  userPermission = UserPermission; // uprawnienia uzytkownika
  userId = 0; // id uzytkownika

  @Input() typeNotification: TypeNotification = 0; // typ notyfikacji

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatTable, { static: true }) table: MatTable<any>;

  constructor(
    private router: Router,
    private dialog: MatDialog,
    private location: Location,
    private activatedRoute: ActivatedRoute,
    private snackBarService: SnackBarService,
    private storageUserService: StorageUserService,
    private notificationService: NotificationService,
    private notificationHttpService: NotificationHttpService,
    public roleGuardService: RoleGuardService,
    public notificationStatusService: NotificationStatusService
  ) {}

  ngOnInit() {
    this.detectParamUserId();
    this.getNotificationsByType(this.typeNotification);
  }

  /**
   * Filtrowanie tabeli
   */
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  /** Przypisywanie osoby do zgłoszenia */
  openAssignUserDialog(row: Notification) {
    const dialogRef = this.dialog.open(NotificationAssignDialogComponent, {
      width: '450px',
      data: {},
    });

    dialogRef.afterClosed().subscribe((result: User) => {
      if (result) {
        const updatedNotification = new UpdateNotification(row);
        updatedNotification.assignUser(result.id);
        this.notificationHttpService.updateNotification(updatedNotification).subscribe(
          (next) => {
            row.assignedUser = result; // przypisywanie uzytkownika
            row.status = row.status === 0 ? StatusNotification.Przypisane_do_użytkownika : row.status;
            this.table.renderRows();
            this.snackBarService.openSnackBar('Udana aktualizacja zgłoszenia', '', SnackBarStatusAction.Success);
          },
          (error) => {
            this.snackBarService.openSnackBar('Nieudana aktualizacja zgłoszenia', '', SnackBarStatusAction.Error);
          }
        );
      }
    });
  }

  /** Zmiana statusu zgloszenia */
  openChangeNotificationStatus(notification: Notification): any {
    const dialogRef = this.dialog.open(NotificationChangeStatusDialogComponent, {
      data: { notification },
      width: '500px',
      hasBackdrop: true,
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        const updatedNotification = new UpdateNotification(notification);
        updatedNotification.changeStatus(result);
        this.notificationHttpService.updateNotification(updatedNotification).subscribe(
          (next) => {
            this.snackBarService.openSnackBar(
              `Udane aktualizacja statusu ${notification.title}`,
              '',
              SnackBarStatusAction.Success
            );
            notification.status = result;
            this.table.renderRows();
          },
          (error) => {
            this.snackBarService.openSnackBar('Nieudana aktualizacja statusu', '', SnackBarStatusAction.Error);
          }
        );
      }
    });
  }

  /** Oznaczanie zgłoszenia jako ukończony */
  openConfirmCompletingTheTask(row: Notification): any {
    const header = 'Ukończenie zadania';
    const content = 'Czy aby napewno chcesz ukończyć zadanie?';

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: { header, content },
      hasBackdrop: true,
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        const updatedNotification = new UpdateNotification(row);
        updatedNotification.changeStatus(StatusNotification.Rozwiazane);
        this.notificationHttpService.updateNotification(updatedNotification).subscribe((next) => {
          this.table.renderRows();
          this.snackBarService.openSnackBar(`Pomyślnie ukończono ${row.title}`, '', SnackBarStatusAction.Success);
        });
      }
    });
  }

  /** Informacje o zgłoszeniu */
  openNotificationMoreInfo(notification: Notification): any {
    this.notificationService.setCurrentSelectNotification(notification);
    this.router.navigate([`/pages/notifications/detail`, notification.id], {
      relativeTo: this.activatedRoute,
    });
  }

  /** Wykrywanie z jakim id uzytkownika z url badz serwisu */
  detectParamUserId(): boolean {
    const userIdTmp = this.activatedRoute.snapshot.queryParamMap.get('userId');

    const regexPublished = new RegExp('notifications/published');
    const regexAssigned = new RegExp('notifications/assigned');
    const url = this.router.url;

    if (regexAssigned.test(url)) {
      this.header = 'Utworzone zgłoszenia przez użytkownika';
    } else if (regexPublished.test(url)) {
      this.header = 'Przypisane zgłoszenia do użytkownika';
    }

    if (userIdTmp !== null) {
      this.userId = parseInt(userIdTmp, 10);
      return true;
    } else {
      this.userId = this.storageUserService.getLoggedUser().userId;
      return false;
    }
  }

  /** Powrót do poprzedniej lokalizacji */
  goBack() {
    this.location.back();
  }

  /** Pobieranie zgłoszeń na podstawie otwartej karty */
  private getNotificationsByType(type: TypeNotification) {
    switch (type) {
      case TypeNotification.All: {
        this.getAllNotifications();
        break;
      }
      case TypeNotification.Assigned: {
        this.getAssignedNotifications(this.userId);
        break;
      }
      case TypeNotification.Published: {
        this.getPublishedNotifications(this.userId);
        break;
      }
    }
  }

  /** Pobieranie wszystkich zgłoszeń */
  private getAllNotifications() {
    this.notificationHttpService.getNotifications().subscribe(
      (next) => {
        this.notifications = [];
        next.forEach(src => {
          const notification:Notification = new Notification();
          notification.copyValues(src);
          notification.assignedUser = src.assignedUser;
          this.notifications.push(notification);
        });
      },
      (error) => {
        this.snackBarService.openSnackBar('Nieudane pobranie zgłoszeń', '', SnackBarStatusAction.Error);
      },
      () => {
        this.dataSource = new MatTableDataSource(this.notifications);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    );
  }

  /** Pobieranie przypisanych zgłoszeń dla danej osoby */
  private getAssignedNotifications(userId: number) {
    this.notificationHttpService.getNotificationsByAssignedUserId(userId).subscribe(
      (next) => {
        this.notifications = [];
        next.forEach(src => {
          const notification:Notification = new Notification();
          notification.copyValues(src);
          notification.assignedUser = src.assignedUser;
          this.notifications.push(notification);
        });
      },
      (error) => {
        this.snackBarService.openSnackBar('Nieudane pobranie zgłoszeń', '', SnackBarStatusAction.Error);
      },
      () => {
        this.dataSource = new MatTableDataSource(this.notifications);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    );
  }

  /** Pobieranie opublikowanych zgłoszeń */
  private getPublishedNotifications(userId: number) {
    this.notificationHttpService.getNotificationsByPublishedUserId(userId).subscribe(
      (next) => {
        this.notifications = [];
        next.forEach(src => {
          const notification:Notification = new Notification();
          notification.copyValues(src);
          notification.assignedUser = src.assignedUser;
          this.notifications.push(notification);
        });
      },
      (error) => {
        this.snackBarService.openSnackBar('Nieudane pobranie zgłoszeń', '', SnackBarStatusAction.Error);
      },
      () => {
        this.dataSource = new MatTableDataSource(this.notifications);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    );
  }
}
