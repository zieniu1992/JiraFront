import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DragAndDropComponent } from './drag-and-drop.component';
import { AngularMaterialModule } from 'src/app/_modules/angular-material.module';



@NgModule({
  declarations: [
    DragAndDropComponent
  ],
  imports: [
    CommonModule,
    AngularMaterialModule
  ],
  exports: [
    DragAndDropComponent
  ]
})
export class DragAndDropModule { }
