// Modules
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AngularMaterialModule } from 'src/app/_modules/angular-material.module';

// Components
import { ToolbarComponent } from '.';
import { UserBottomListModule } from '../../pages/users/helpers/user-bottom-list/user-bottom-list.module';
import { NotificationService } from 'src/app/_services/notification/notification.service';

@NgModule({
  declarations: [
    ToolbarComponent,
  ],
  imports: [
    CommonModule,
    UserBottomListModule,
    AngularMaterialModule
  ],
  exports: [
    ToolbarComponent
  ],
  providers: [
    NotificationService
  ]
})
export class ToolbarModule { }
