import { Pipe, PipeTransform } from '@angular/core';
import { StatusNotification } from 'src/app/_models/notification/helpers/statusNotification';

@Pipe({
  name: 'statusNotificationNumberToStr',
})
export class StatusNotificationNumberToStrPipe implements PipeTransform {

  transform(value: number): string {
const replace = /_/g;

const status = StatusNotification[value];

    return status.replace(replace,' ');
  }
}
