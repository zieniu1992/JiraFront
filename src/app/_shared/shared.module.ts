import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { EnumPermissionToStrPipe } from './pipe/enum-permission-to-str.pipe';
import { EnumToArrayPipe } from './pipe/enum-to-array.pipe';
import { TypeNotificationPipe } from './pipe/notification/type-notification.pipe';
import { StatusNotificationNumberToStrPipe } from './pipe/notification/status-notification-number-to-str.pipe';
import { UserStatusPipe } from './pipe/user/user-status.pipe';
import { OrderByDatePipe } from './pipe/order-by-date.pipe';

@NgModule({
  declarations: [
    OrderByDatePipe,
    UserStatusPipe,
    EnumToArrayPipe,
    TypeNotificationPipe,
    EnumPermissionToStrPipe,
    StatusNotificationNumberToStrPipe,
  ],
  imports: [CommonModule],
  exports: [
    UserStatusPipe,
    OrderByDatePipe,
    EnumToArrayPipe,
    TypeNotificationPipe,
    EnumPermissionToStrPipe,
    StatusNotificationNumberToStrPipe,
  ],
  providers:[
    DatePipe,
    UserStatusPipe,
    EnumPermissionToStrPipe
  ]
})
export class SharedModule {
}
