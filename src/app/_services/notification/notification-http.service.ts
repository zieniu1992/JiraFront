import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Notification } from '../../_models/notification/notification';
import { UpdateNotification } from 'src/app/_models/notification/updateNotification';
import { NewNotification } from 'src/app/_models/notification/newNotification';

@Injectable({
  providedIn: 'root',
})
export class NotificationHttpService {
  constructor(private http: HttpClient) {}

  linkHttp = `${environment.apiUrl}api/notification/`;

  /** Pobieranie wszystkich zgłoszeń */
  getNotifications(): Observable<Array<Notification>> {
    return this.http.get<Notification[]>(this.linkHttp);
  }

  /** Pobieranie zgłoszeń przypisanych do daneej osoby */
  getNotificationsByAssignedUserId(userId: number): Observable<Array<Notification>> {
    return this.http.get<Notification[]>(this.linkHttp + 'assigned/' + userId);
  }

  /** Pobieranie zgłoszeń stworzoną przez dana osobę */
  getNotificationsByPublishedUserId(userId: number): Observable<Array<Notification>> {
    return this.http.get<Notification[]>(this.linkHttp + 'published/' + userId);
  }

  /** Tworzenie nowego zgloszenia */
  createNotification(notification: NewNotification) {
    return this.http.post(this.linkHttp, notification);
  }

  /** Aktualizacja zgłoszenia */
  updateNotification(notification: UpdateNotification) {
    return this.http.put(this.linkHttp + notification.id, notification);
  }

  /** Usuwanie zgłoszenia */
  deleteNotification(id: number) {
    return this.http.delete(this.linkHttp + id);
  }
}
