import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  constructor(private http: HttpClient, private router: Router) {}

  /** Signed to application normal registration */
  signIn(email: string, password: string) {
    return this.http
      .post<any>(`${environment.apiUrl}api/user/authenticate`, { email, password })
      .pipe(
        map((user) => {
          // login successful if there's a jwt token in the response
          if (user && user.accessToken) {
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            sessionStorage.setItem('currentUser', JSON.stringify(user));
          }
          return user;
        })
      );
  }

  /** Remove user from local storage to log user out */
  logout() {
    sessionStorage.clear();
    this.router.navigateByUrl('/login');
  }
}
